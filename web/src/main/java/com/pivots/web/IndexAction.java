package com.pivots.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.pivots.spi.AppService;
import com.pivots.spi.dto.ArticleDTO;

public class IndexAction extends ActionSupport {

	private static final long serialVersionUID = 3351876781777309506L;

	private AppService app;

	public void setApp(AppService app) {
		this.app = app;
	}

	List<ArticleDTO> list;

	public List<ArticleDTO> getList() {
		return list;
	}

	public void setList(List<ArticleDTO> list) {
		this.list = list;
	}

	public String execute() throws Exception {

		List<ArticleDTO> dtos = new ArrayList<ArticleDTO>();
		HttpServletRequest req = ServletActionContext.getRequest();
//		String typee = req.getParameter("type");
		String searchString = req.getParameter("searchString");
		if (searchString != null) {
//			if ("1".equals(typee)) {
				dtos = app.getArticlesFromSearchString(searchString);
//			} else {
//				String articleType = req.getParameter("articleType");
//				dtos = app.getArticlesFromType(articleType);
//			}
		}
		setList(dtos);
		return SUCCESS;
	}

}
