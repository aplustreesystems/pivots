<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<meta charset="utf-8">
<title>Medical Information...</title>
<link rel="stylesheet" href="../css/jquery-ui.css">
<script src="../js/jquery-1.10.2.js"></script>
<script src="../js//jquery-ui.js"></script>

<style type="text/css">
body {
	font-family: "Trebuchet MS", "Helvetica", "Arial", "Verdana",
		"sans-serif";
	font-size: 62.5%;
}
.lnk {
    color: blue;
    text-decoration: underline;
    cursor: pointer;
}

</style>


<script type="text/javascript">

$(function() {
	  
	  $("#lnkId").click(function(e){

		$.ajax({
		    url: "./get",
		    dataType: "json",
		    data: {
		         searchString: $("#search").val(),
		         type : $("rd1").val()
		      },
		    success: function( data ) {
		    	
		    	$("#content").empty();
		  	 	//$("#title").html("<p>"+data.title+"<p>");
		  	 	//$("#content").html(data.content);
				//alert(JSON.stringify(data));
				var dtos = data.list;
				var index = 0;
				$.each(dtos, function(){
					
					$("#content").add('div').attr("id",index+"div");
					$("#"+index+"div").append(dtos[index].content);
					//alert(JSON.stringify(dtos[index].content));
					alert(index);
					index++;
					
				});
		    },
		
		    error: function(XMLHttpRequest, textStatus, errorThrown){
		        alert('error - ' +errorThrown);
		        console.log('error', textStatus, errorThrown);
		    }
		  });

		
	  });
});
</script>
</head>
<body>
	<h1>Articles</h1>

	<s:form id="main" action="">

		<s:radio label="Search By Article Type" name="yourAnswer" id="rd1"
			list="#{'1':'Yes','2':'No'}" value="2" />
		<s:textfield name="search" label="Search" id="search" />

		<%-- <s:textfield name="result" id="result"  value=""/> --%>
	</s:form>
	
	<s:div>
		<span class="lnk" id="lnkId">SEARCH...</span>
	</s:div>
	<br>
	<br>
	<s:div />
	
	<div id="title"></div>
	<br />
	<div id="content"></div>
</body>
</html>
