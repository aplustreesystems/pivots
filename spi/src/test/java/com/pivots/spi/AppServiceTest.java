package com.pivots.spi;

import java.util.List;

import junit.framework.TestCase;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.pivots.dao.entity.Article;
import com.pivots.dao.entity.Author;
import com.pivots.spi.dto.ArticleDTO;

/**
 * The Class AppServiceTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:applicationContext.xml" })
public class AppServiceTest extends TestCase{

	/** The srv. */
	@Autowired
	AppService srv;
	
	/**
	 * Gets the articles from type test.
	 *
	 * @return the articles from type test
	 */
	@Test
	public void getArticlesFromTypeTest(){
		
		List<ArticleDTO> list = null;
		list = srv.getArticlesFromType("2");
		assertTrue(list!=null);
		assertTrue(list.size()>0);
		for (ArticleDTO article : list) {
			System.out.println(article.getContent());
		}
	}
	
	/**
	 * Gets the article from search string test.
	 *
	 * @return the article from search string test
	 */
	@Test
	public void getArticleFromSearchStringTest(){
		
		List<ArticleDTO> list = null;
		list = srv.getArticlesFromSearchString("follow");
		System.out.println("here..................."+list.size());
		assertTrue(list!=null);
		assertTrue(list.size()>0);
		for (ArticleDTO article : list) {
			
			System.out.println(article.getContent());
		}
	}

}
