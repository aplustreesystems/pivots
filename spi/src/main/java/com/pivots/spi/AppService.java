package com.pivots.spi;

import java.util.List;

import com.pivots.dao.Dao;
import com.pivots.dao.entity.Article;
import com.pivots.spi.dto.ArticleDTO;
import com.pivots.spi.utils.Converter;

/**
 * The Class AppService.
 */
public class AppService{
	
	/** The dao. */
	Dao dao;
	
	/**
	 * Sets the dao.
	 *
	 * @param dao the new dao
	 */
	public void setDao(Dao dao) {
		this.dao = dao;
	}

	/**
	 * Gets the articles from type.
	 *
	 * @param typeId the type id
	 * @return the articles from type
	 */
	public List<ArticleDTO> getArticlesFromType(String typeId){
		
		List<Article> list = dao.findArticlesByType(Integer.valueOf(typeId));
		return Converter.convert(list);
	}

	/**
	 * Gets the articles from search string.
	 *
	 * @param searchString the search string
	 * @return the articles from search string
	 */
	public List<ArticleDTO> getArticlesFromSearchString(String searchString){
		
		List<Article> list = dao.execFullTextSearchOnArticle(searchString);
		return Converter.convert(list);
	}

	
}
