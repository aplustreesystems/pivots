package com.pivots.spi.utils;

import java.util.ArrayList;
import java.util.List;

import com.pivots.dao.entity.Article;
import com.pivots.spi.dto.ArticleDTO;

/**
 * The Class Converter.
 */
public class Converter {

	/**
	 * Convert.
	 *
	 * @param article the article
	 * @return the article dto
	 */
	public static ArticleDTO convert(Article article){
		
		ArticleDTO dto = new ArticleDTO();
		dto.setContent(article.getContent()).setTitle(article.getTitle())
			.setEmail(article.getAuthor().getEmail()).setName(article.getAuthor().getName())
			.setLocation(article.getAuthor().getLocation());
		return dto;
	}
	

	/**
	 * Convert.
	 *
	 * @param articles the articles
	 * @return the list
	 */
	public static List<ArticleDTO> convert(List<Article> articles){
		
		List<ArticleDTO> list = new ArrayList<ArticleDTO>();
		for (Article article : articles) {
			list.add(convert(article));
		}
		return list;
	}

}
