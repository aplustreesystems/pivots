package com.pivots.spi.dto;

import java.io.Serializable;

/**
 * The Class ArticleDTO.
 */
public class ArticleDTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3748850879299436462L;
	
	/** The name. */
	private String name;
	
	/** The email. */
	private String email;
	
	/** The location. */
	private String location;
	
	/** The title. */
	private String title;
	
	/** The content. */
	private String content;
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the name
	 * @return the article dto
	 */
	public ArticleDTO setName(String name) {
		this.name = name;
		return this;
	}
	
	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * Sets the email.
	 *
	 * @param email the email
	 * @return the article dto
	 */
	public ArticleDTO setEmail(String email) {
		this.email = email;
		return this;
	}
	
	/**
	 * Gets the location.
	 *
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}
	
	/**
	 * Sets the location.
	 *
	 * @param location the location
	 * @return the article dto
	 */
	public ArticleDTO setLocation(String location) {
		this.location = location;
		return this;
	}
	
	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Sets the title.
	 *
	 * @param title the title
	 * @return the article dto
	 */
	public ArticleDTO setTitle(String title) {
		this.title = title;
		return this;
	}
	
	/**
	 * Gets the content.
	 *
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	
	/**
	 * Sets the content.
	 *
	 * @param content the content
	 * @return the article dto
	 */
	public ArticleDTO setContent(String content) {
		this.content = content;
		return this;
	}
}
