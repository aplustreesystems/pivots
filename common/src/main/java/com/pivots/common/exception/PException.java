package com.pivots.common.exception;

/**
 * The Class PException.
 */
public class PException extends RuntimeException{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3993936681616405967L;

	/**
	 * Instantiates a new p exception.
	 */
	public PException() {
		super();
	}

	/**
	 * Instantiates a new p exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 * @param enableSuppression the enable suppression
	 * @param writableStackTrace the writable stack trace
	 */
	public PException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Instantiates a new p exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public PException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new p exception.
	 *
	 * @param message the message
	 */
	public PException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new p exception.
	 *
	 * @param cause the cause
	 */
	public PException(Throwable cause) {
		super(cause);
	}
	
}
