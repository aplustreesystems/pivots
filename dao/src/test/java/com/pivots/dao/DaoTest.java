package com.pivots.dao;

import java.util.List;

import junit.framework.TestCase;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.pivots.dao.entity.Article;
import com.pivots.dao.entity.Author;

/**
 * The Class DaoTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:applicationContext.xml" })
public class DaoTest extends TestCase{

	/** The dao. */
	@Autowired
	Dao dao;
	
	/** The session factory. */
	@Autowired
	SessionFactory sessionFactory;
	

	/**
	 * Test find author by id.
	 */
	@Test
	public void testFindAuthorById(){
		
		System.out.println(">>>>>>>>>>>>>>>>.");
		Author auth = dao.findAuthorById("4");
		assertTrue(auth!=null);

		System.out.println(auth);
	}
	
	/**
	 * Test exec hql.
	 */
	@Test
	public void testExecHQL(){
		
		List<Article> list = null;
		list = dao.findArticlesByType(1);
		assertTrue(list!=null);
		assertTrue(list.size()>0);
		for (Article article : list) {
			System.out.println(article);
		}
	}
	
	
	/**
	 * Test all.
	 */
	@Test
	public void testAll(){

		List<Article> list = null;
		list = dao.findAllArticles();
		assertTrue(list!=null);
		assertTrue(list.size()>0);
		for (Article article : list) {
			System.out.println(article);
		}

	}
	
	/**
	 * Creates the indexes test.
	 */
	@Test
	public void createIndexesTest(){
		System.out.println("starting........");
		dao.builder();
		System.out.println("Ending........");
	}
	
	/**
	 * Search articles test.
	 */
	@Test
	public void searchArticlesTest(){
		
		List<Article> list = null;
		list = dao.execFullTextSearchOnArticle("follow");
		assertTrue(list!=null);
		assertTrue(list.size()>0);
		System.out.println("here..................."+list.size());
		for (Article article : list) {
			
			System.out.println(article);
		}
	}

}
