package com.pivots.dao.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.annotations.Field;

/**
 * The Class Author.
 */
@Entity
@Table(name = "AUTHOR")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class Author implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7187225468966723547L;
	
	/** The auth id. */
	private int authId;
	
	/** The name. */
	private String name;
	
	/** The email. */
	private String email;
	
	/** The location. */
	private String location;
	
	/** The articles. */
	private Set<Article> articles = new HashSet<Article>(0);

	
	/**
	 * Gets the auth id.
	 *
	 * @return the auth id
	 */
	@Id
	@Column(name = "auth_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getAuthId() {
		return authId;
	}
	
	/**
	 * Sets the auth id.
	 *
	 * @param authId the new auth id
	 */
	public void setAuthId(int authId) {
		this.authId = authId;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	@Field
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	@Field
	public String getEmail() {
		return email;
	}
	
	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * Gets the location.
	 *
	 * @return the location
	 */
	@Field
	public String getLocation() {
		return location;
	}
	
	/**
	 * Sets the location.
	 *
	 * @param location the new location
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	
	/**
	 * Gets the articles.
	 *
	 * @return the articles
	 */
	@OneToMany(fetch=FetchType.LAZY , mappedBy="author")
	public Set<Article> getArticles() {
		return articles;
	}
	
	/**
	 * Sets the articles.
	 *
	 * @param articles the new articles
	 */
	public void setArticles(Set<Article> articles) {
		this.articles = articles;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		
		return "name: "+name+" email :"+email+" location:"+location;
	}
}
