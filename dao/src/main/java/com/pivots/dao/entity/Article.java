package com.pivots.dao.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.solr.analysis.LowerCaseFilterFactory;
import org.apache.solr.analysis.SnowballPorterFilterFactory;
import org.apache.solr.analysis.StandardTokenizerFactory;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

// TODO: Auto-generated Javadoc
/**
 * The Class Article.
 */
@Entity
@Table(name = "ARTICLE")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Indexed
@AnalyzerDef(name="test", tokenizer = @TokenizerDef (factory = StandardTokenizerFactory.class)
		, filters = { 
			@TokenFilterDef (factory = LowerCaseFilterFactory.class)
		   ,@TokenFilterDef (factory = SnowballPorterFilterFactory.class ,params = { 
			   @Parameter (name = "language" , value="English")
		   })	
})
			
public class Article implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5765516912448215041L;
	
	/** The id. */
	private int id;
	
	/** The author. */
	private Author author;
	
	/** The title. */
	private String title;
	
	/** The type. */
	private int type;
	
	/** The content. */
	private String content;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Gets the author.
	 *
	 * @return the author
	 */
	@ManyToOne(fetch= FetchType.EAGER)
	@JoinColumn(name="AUTH_ID" , nullable=false)
	@IndexedEmbedded
	public Author getAuthor() {
		return author;
	}
	
	/**
	 * Sets the author.
	 *
	 * @param author the new author
	 */
	public void setAuthor(Author author) {
		this.author = author;
	}
	
	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	@Field(index = Index.YES,analyze=Analyze.YES,store=Store.NO)
	@Analyzer(definition = "test")
	public String getTitle() {
		return title;
	}
	
	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	
	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(int type) {
		this.type = type;
	}
	
	/**
	 * Gets the content.
	 *
	 * @return the content
	 */
	@Field(index=Index.YES,analyze=Analyze.YES,store=Store.NO)
	@Analyzer(definition = "test")
	public String getContent() {
		return content;
	}
	
	/**
	 * Sets the content.
	 *
	 * @param content the new content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		
		return "id : "+id+" author : "+author.toString()+" title : "+title +" content: "+content ;
	}
}
