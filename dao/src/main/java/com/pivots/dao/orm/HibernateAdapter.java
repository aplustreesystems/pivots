package com.pivots.dao.orm;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;


/**
 * The Class HibernateAdapter.
 */
public class HibernateAdapter {
	
	/** The sf. */
	private SessionFactoryImpl sf;

	
	/**
	 * Sets the session factory.
	 *
	 * @param sf the new session factory
	 */
	public void setSessionFactory(SessionFactoryImpl sf) {
		this.sf = sf;
	}

	/**
	 * Gets the session factory.
	 *
	 * @return the session factory
	 */
	public SessionFactoryImpl getSessionFactory() {
		return this.sf;
	}

	/**
	 * Gets the hib session.
	 *
	 * @return the hib session
	 */
	public Session getHibSession(){
		return sf.getCurrentSession();
	}
	
	/**
	 * Find by id.
	 *
	 * @param <T> the generic type
	 * @param className the class name
	 * @param id the id
	 * @return the t
	 */
	@SuppressWarnings("unchecked")
	public <T> T findById(String className,int id){
		
		T obj = null;
		obj = (T)getHibSession().get(className, id);
		return obj;
	}
	
	/**
	 * Exec hql.
	 *
	 * @param <T> the generic type
	 * @param entity the entity
	 * @param paramNameType the param name type
	 * @param hql the hql
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public <T> List<T> execHQL( T entity,
			Map<String, Serializable> paramNameType,
			String hql) {
		List<T> projectedResult = null;
		Session session = getHibSession();
		Query query = session.createQuery(hql);
		query.setProperties(entity);
		if(paramNameType != null){
			for (Map.Entry<String, Serializable> entry : paramNameType
					.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}
		}
		projectedResult = query.list();
		return projectedResult;
	}

	
	
	/**
	 * Exec full text search.
	 *
	 * @param <T> the generic type
	 * @param entity the entity
	 * @param searchString the search string
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public <T> List <T> execFullTextSearch(T entity,String searchString){
		
		List<T> projectedResult = null;
		FullTextSession full = Search.getFullTextSession(getHibSession());
		Transaction tx = full.beginTransaction();
		try {
			QueryBuilder qb = full.getSearchFactory().buildQueryBuilder()
					.forEntity(entity.getClass()).get();
			
			org.apache.lucene.search.Query query = qb.keyword()
					.onFields("title","content","author.name","author.email","author.location")
					.matching(searchString).createQuery();
			
			FullTextQuery fQuery = full.createFullTextQuery(query, entity.getClass());
			projectedResult = fQuery.list();
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		}
		return projectedResult;
	}

	/**
	 * Builds the indexes.
	 */
	public void buildIndexes(){
		FullTextSession fullTextSession = Search.getFullTextSession(getHibSession());
		try {
			fullTextSession.createIndexer().startAndWait();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

}

