package com.pivots.dao.orm;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import com.pivots.dao.entity.Article;
import com.pivots.dao.entity.Author;

@Deprecated
@SuppressWarnings("deprecation")
public class DBUtils {
	
	public static final SessionFactory session = getConfiguration().
			configure("hibernate.cfg.xml").buildSessionFactory();

	@SuppressWarnings("unused")
	private static Configuration getConfiguration(){
		
		AnnotationConfiguration conf = new AnnotationConfiguration();
		conf.addPackage("com.pivots.dal.entity")
		.addAnnotatedClass(Author.class)
		.addAnnotatedClass(Article.class);
		return conf;
	}
	
}
