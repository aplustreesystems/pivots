package com.pivots.dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;

import com.pivots.dao.entity.Article;
import com.pivots.dao.entity.Author;
import com.pivots.dao.orm.HibernateAdapter;

/**
 * The Class Dao.
 */
public class Dao {
	
	/** The session factory. */
	private SessionFactoryImpl sessionFactory;

	/** The hibernate adapter. */
	private HibernateAdapter hibernateAdapter;

	/**
	 * Gets the hibernate adapter.
	 *
	 * @return the hibernate adapter
	 */
	public HibernateAdapter getHibernateAdapter() {
		return hibernateAdapter;
	}

	/**
	 * Sets the hibernate adapter.
	 *
	 * @param hibernateAdapter the new hibernate adapter
	 */
	public void setHibernateAdapter(HibernateAdapter hibernateAdapter) {
		this.hibernateAdapter = hibernateAdapter;
	}

	/**
	 * Sets the session factory.
	 *
	 * @param sf the new session factory
	 */
	public void setSessionFactory(SessionFactoryImpl sf) {
		this.sessionFactory = sf;
	}

	/**
	 * Gets the session factory.
	 *
	 * @return the session factory
	 */
	public SessionFactoryImpl getSessionFactory() {
		return this.sessionFactory;
	}

	/**
	 * Gets the hibernate session.
	 *
	 * @return the hibernate session
	 */
	private Session getHibernateSession() {
		return sessionFactory.getCurrentSession();
	}

	
	/**
	 * Find author by id.
	 *
	 * @param id the id
	 * @return the author
	 */
	public Author findAuthorById(String id){
		
		Author auth = new Author();
		Transaction tx  =getHibernateSession().beginTransaction();
		try {
			auth = hibernateAdapter.findById(Author.class.getName(), Integer.valueOf(id));
			tx.commit();
		}catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		}

		return auth;
	}
	
	/**
	 * Find article by id.
	 *
	 * @param id the id
	 * @return the article
	 */
	public Article findArticleById(String id){
		
		Article article = new Article();
		Transaction tx = getHibernateSession().beginTransaction();	
		
		try{
			article = hibernateAdapter.findById(Article.class.getName(), Integer.valueOf(id));
			tx.commit();
		}catch(Exception ex){
			tx.rollback();
			throw new RuntimeException(ex);
		}
		return article;
	}
	
	/**
	 * Find articles by type.
	 *
	 * @param type the type
	 * @return the list
	 */
	public List<Article> findArticlesByType(int type){
		
		List<Article> list = null;
		Article article = new Article();
		Transaction tx = getHibernateSession().beginTransaction();
		try{
			Map<String, Serializable> map = new HashMap<String, Serializable>(1);
			map.put("type", type);
			list = hibernateAdapter.execHQL(article, map , "from Article as a where a.type = :type");
			for (Article article2 : list) {
				Hibernate.initialize(article2.getAuthor());
			}
//			if(list.size()>0)
			tx.commit();
		}catch(Exception ex){
			tx.rollback();
			throw new RuntimeException(ex);
		}
		return list;
	}

	
	/**
	 * Find all articles.
	 *
	 * @return the list
	 */
	public List<Article> findAllArticles(){
		
		List<Article> list = null;
		Article article = new Article();
		Transaction tx = getHibernateSession().beginTransaction();
		try{
			list = hibernateAdapter.execHQL(article, null, "from Article as a ");
			for (Article article2 : list) {
				Hibernate.initialize(article2.getAuthor());
			}
			tx.commit();
		}catch(Exception ex){
			tx.rollback();
			throw new RuntimeException(ex);
		}
		return list;
	}

	/**
	 * Search articles.
	 *
	 * @param searchString the search string
	 * @return the list
	 */
	public List<Article> searchArticles(String searchString){
		
		List<Article> list = null;
		Article article = new Article();
		try {
			list = hibernateAdapter.execFullTextSearch(article, searchString);
		} catch (RuntimeException e) {
			throw e;
		}
		return list;
	}
	
	/**
	 * Exec full text search on article.
	 *
	 * @param searchString the search string
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List <Article> execFullTextSearchOnArticle(String searchString){
		
		List<Article> projectedResult = null;
		FullTextSession full = Search.getFullTextSession(getHibernateSession());
		Transaction tx = full.beginTransaction();
		try {
			QueryBuilder qb = full.getSearchFactory().buildQueryBuilder()
					.forEntity(Article.class).get();
			
			org.apache.lucene.search.Query query = qb.keyword()
					.onFields("title","content","author.name","author.email","author.location")
					.matching(searchString).createQuery();
			
			FullTextQuery fQuery = full.createFullTextQuery(query, Article.class);
			projectedResult = fQuery.list();
			for (Article article : projectedResult) {
				Hibernate.initialize(article.getAuthor());
			}
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			throw new RuntimeException(e);
		}
		return projectedResult;
	}

	
	/**
	 * Builder.
	 */
	public void builder(){
		hibernateAdapter.buildIndexes();
	}
}
